package br.com.sicredi.simulacao.tests.simulacoes.test;

import br.com.sicredi.simulacao.tests.base.BaseTest;
import br.com.sicredi.simulacao.tests.simulacoes.request.SimulacaoRequest;
import br.com.sicredi.simulacao.utils.Utils;
import org.apache.http.HttpStatus;
import org.junit.jupiter.api.*;

import java.util.concurrent.TimeUnit;

import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.Matchers.lessThan;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class CriarSimulacoesTest extends BaseTest {

    SimulacaoRequest simulacao = new SimulacaoRequest();

    @Test
    @Tag("todos")
    @Order(3)
    public void deveCadastrarSimulacaoComSucesso() {
        simulacao.criar(false, true)
                 .then()
                    .statusCode(HttpStatus.SC_CREATED)
                    .time(lessThan(2L), TimeUnit.SECONDS)
                    .log().all();
    }

    @Test
    @Tag("todos")
    @Order(10)
    public void deveValidarCadastroSimulacaoComCpfComPontos() {
        simulacao.criar(true, true)
                .then()
                .statusCode(HttpStatus.SC_CREATED)
                .time(lessThan(2L), TimeUnit.SECONDS)
                .log().all();
    }

    @Test
    @Tag("todos")
    @Order(11)
    public void deveValidarCadastroSimulacaoComEmailInvalido() {
        simulacao.criar(false, "email-invalido.com", true)
                .then()
                .statusCode(HttpStatus.SC_BAD_REQUEST)
                .body("erros.email", anyOf(equalTo("E-mail deve ser um e-mail válido"), equalTo("não é um endereço de e-mail")))
                .time(lessThan(2L), TimeUnit.SECONDS)
                .log().all();
    }

    @Test
    @Tag("todos")
    @Order(12)
    public void deveValidarCadastroSimulacaoComValorAcima() {
        simulacao.criar(false, 50000.00, true)
                .then()
                .statusCode(HttpStatus.SC_BAD_REQUEST)
                .body("erros.valor", equalTo("Valor deve ser menor ou igual a R$ 40.000"))
                .time(lessThan(2L), TimeUnit.SECONDS)
                .log().all();
    }

    @Test
    @Tag("todos")
    @Order(13)
    public void deveValidarCadastroSimulacaoComValorAbaixo() {
        simulacao.criar(false, 100.00, true)
                .then()
                .statusCode(HttpStatus.SC_CREATED)
                .time(lessThan(2L), TimeUnit.SECONDS)
                .log().all();
    }

    @Test
    @Tag("todos")
    @Order(14)
    public void deveValidarCadastroSimulacaoComParcelaAcima() {
        simulacao.criar(false, 49, true)
                .then()
                .statusCode(HttpStatus.SC_CREATED)
                .time(lessThan(2L), TimeUnit.SECONDS)
                .log().all();
    }

    @Test
    @Tag("todos")
    @Order(15)
    public void deveValidarCadastroSimulacaoComParcelaAbaixo() {
        simulacao.criar(false, 1, true)
                .then()
                .statusCode(HttpStatus.SC_BAD_REQUEST)
                .body("erros.parcelas", equalTo("Parcelas deve ser igual ou maior que 2"))
                .time(lessThan(2L), TimeUnit.SECONDS)
                .log().all();
    }

    @Test
    @Tag("todos")
    @Order(16)
    public void deveValidarCadastroSimulacaoComCpfExistente() {
        simulacao.criar(Utils.cpfValido(), true)
                .then()
                .statusCode(HttpStatus.SC_BAD_REQUEST)
                .body("mensagem", equalTo("CPF duplicado"))
                .time(lessThan(2L), TimeUnit.SECONDS)
                .log().all();
    }

    @Test
    @Tag("todos")
    @Order(17)
    public void deveValidarCadastroSimulacaoComSeguroFalse() {
        simulacao.criar(false, false)
                .then()
                .statusCode(HttpStatus.SC_CREATED)
                .time(lessThan(2L), TimeUnit.SECONDS)
                .log().all();
    }
}
