package br.com.sicredi.simulacao.tests.restricoes.request;

import io.restassured.response.Response;

import static io.restassured.RestAssured.given;

public class RestricoesRequest {

    private final String PATH_RESTRICOES = "/restricoes";

    public Response buscarComRestricao(String cpfComRestricao) {
        return given()
                    .pathParam("cpf", cpfComRestricao)
               .when()
                    .log().all()
                    .get(PATH_RESTRICOES + "/{cpf}");
    }

    public Response buscarSemRestricao(String cpfSemRestricao) {
        return given()
                    .pathParam("cpf", cpfSemRestricao)
               .when()
                    .log().all()
                    .get(PATH_RESTRICOES + "/{cpf}");
    }
}
