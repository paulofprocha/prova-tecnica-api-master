package br.com.sicredi.simulacao.tests.restricoes.test;

import br.com.sicredi.simulacao.tests.base.BaseTest;
import br.com.sicredi.simulacao.tests.restricoes.request.RestricoesRequest;
import br.com.sicredi.simulacao.utils.Utils;
import org.apache.http.HttpStatus;
import org.junit.jupiter.api.*;

import java.text.MessageFormat;
import java.util.concurrent.TimeUnit;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.lessThan;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class ListarRestricoesTest extends BaseTest {

    RestricoesRequest restricoes = new RestricoesRequest();

    @Test
    @Tag("todos")
    @Order(6)
    public void deveBuscarCpfComRestricaoComSucesso() {
        String cpfComRestricao = Utils.cpfRestricao();

        restricoes.buscarComRestricao(cpfComRestricao)
                  .then()
                        .statusCode(HttpStatus.SC_OK)
                        .body("mensagem", is(MessageFormat.format("O CPF {0} tem problema", cpfComRestricao)))
                        .time(lessThan(2L), TimeUnit.SECONDS)
                        .log().all();
    }

    @Test
    @Tag("todos")
    @Order(7)
    public void deveBuscarCpfSemRestricaoComSucesso() {
        String cpfSemRestricao = Utils.cpfAleatorio(false);

        restricoes.buscarSemRestricao(cpfSemRestricao)
                  .then()
                        .statusCode(HttpStatus.SC_NO_CONTENT)
                        .time(lessThan(2L), TimeUnit.SECONDS)
                        .log().all();
    }
}
