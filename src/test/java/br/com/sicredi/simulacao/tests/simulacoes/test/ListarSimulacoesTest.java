package br.com.sicredi.simulacao.tests.simulacoes.test;

import br.com.sicredi.simulacao.tests.base.BaseTest;
import br.com.sicredi.simulacao.tests.simulacoes.request.SimulacaoRequest;
import br.com.sicredi.simulacao.utils.Utils;
import org.apache.http.HttpStatus;
import org.junit.jupiter.api.*;

import java.util.concurrent.TimeUnit;

import static io.restassured.module.jsv.JsonSchemaValidator.matchesJsonSchemaInClasspath;
import static org.hamcrest.Matchers.lessThan;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class ListarSimulacoesTest extends BaseTest {

    SimulacaoRequest simulacao = new SimulacaoRequest();

    @Test
    @Tag("todos")
    @Order(1)
    public void deveListarSimulacoesComSucesso() {
        simulacao.listar()
                 .then()
                    .statusCode(HttpStatus.SC_OK)
                    .time(lessThan(2L), TimeUnit.SECONDS)
                    .log().all();
    }

    @Test
    @Tag("todos")
    @Tag("schemas")
    @Order(8)
    public void deveValidarSchemaListarSimulacoes() {
        simulacao.listar()
                .then()
                .statusCode(HttpStatus.SC_OK)
                .body(matchesJsonSchemaInClasspath("schemas/simulacoes/simulacoes.json"))
                .log().all();
    }

    @Test
    @Tag("todos")
    @Order(2)
    public void deveBuscarSimulacaoPorCpfComSucesso() {
        simulacao.buscar(Utils.cpfValido())
                 .then()
                    .statusCode(HttpStatus.SC_OK)
                    .time(lessThan(2L), TimeUnit.SECONDS)
                    .log().all();
    }

    @Test
    @Tag("todos")
    @Tag("schemas")
    @Order(9)
    public void deveValidarSchemaBuscarSimulacaoPorCpf() {
        simulacao.buscar(Utils.cpfValido())
                .then()
                .statusCode(HttpStatus.SC_OK)
                .body(matchesJsonSchemaInClasspath("schemas/simulacoes/simulacao.json"))
                .log().all();
    }
}
