package br.com.sicredi.simulacao.tests.simulacoes.request;

import br.com.sicredi.simulacao.payloads.SimulacoesPayload;
import io.restassured.http.ContentType;
import io.restassured.response.Response;


import static io.restassured.RestAssured.given;

public class SimulacaoRequest {

    private final String PATH_SIMULACOES = "/simulacoes";

    public Response criar(boolean cpfPontos, boolean seguro) {
        return given()
                    .contentType(ContentType.JSON)
                    .body(SimulacoesPayload.toJson(cpfPontos, seguro))
               .when()
                    .log().all()
                    .post(PATH_SIMULACOES);
    }

    public Response criar(boolean cpfPontos, String email, boolean seguro) {
        return given()
                .contentType(ContentType.JSON)
                .body(SimulacoesPayload.toJson(cpfPontos, email, seguro))
                .when()
                .log().all()
                .post(PATH_SIMULACOES);
    }

    public Response criar(boolean cpfPontos, double valor, boolean seguro) {
        return given()
                .contentType(ContentType.JSON)
                .body(SimulacoesPayload.toJson(cpfPontos, valor, seguro))
                .when()
                .log().all()
                .post(PATH_SIMULACOES);
    }

    public Response criar(boolean cpfPontos, int parcelas, boolean seguro) {
        return given()
                .contentType(ContentType.JSON)
                .body(SimulacoesPayload.toJson(cpfPontos, parcelas, seguro))
                .when()
                .log().all()
                .post(PATH_SIMULACOES);
    }

    public Response criar(String cpf, boolean seguro) {
        return given()
                .contentType(ContentType.JSON)
                .body(SimulacoesPayload.toJson(cpf, seguro))
                .when()
                .log().all()
                .post(PATH_SIMULACOES);
    }

    public Response atualizar(String cpf, boolean seguro) {
        return given()
                    .pathParam("cpf", cpf)
                    .contentType(ContentType.JSON)
                    .body(SimulacoesPayload.toJson(cpf, seguro))
               .when()
                    .log().all()
                    .put(PATH_SIMULACOES + "/{cpf}");
    }

    public Response atualizar(String cpf, String email, boolean seguro) {
        return given()
                .pathParam("cpf", cpf)
                .contentType(ContentType.JSON)
                .body(SimulacoesPayload.toJson(cpf, email, seguro))
                .when()
                .log().all()
                .put(PATH_SIMULACOES + "/{cpf}");
    }

    public Response atualizar(String cpf, double valor, boolean seguro) {
        return given()
                .pathParam("cpf", cpf)
                .contentType(ContentType.JSON)
                .body(SimulacoesPayload.toJson(cpf, valor, seguro))
                .when()
                .log().all()
                .put(PATH_SIMULACOES + "/{cpf}");
    }

    public Response listar() {
        return given()
               .when()
                    .log().all()
                    .get(PATH_SIMULACOES);
    }

    public Response buscar(String cpf) {
        return given()
                    .pathParam("cpf", cpf)
               .when()
                    .log().all()
                    .get(PATH_SIMULACOES + "/{cpf}");

    }

    public Response deletar(int id) {
        return given()
                    .pathParam("id", id)
               .when()
                    .log().all()
                    .delete(PATH_SIMULACOES + "/{id}");
    }
}
