package br.com.sicredi.simulacao.tests.simulacoes.test;

import br.com.sicredi.simulacao.tests.base.BaseTest;
import br.com.sicredi.simulacao.tests.simulacoes.request.SimulacaoRequest;
import org.apache.http.HttpStatus;
import org.junit.jupiter.api.*;

import java.util.concurrent.TimeUnit;

import static org.hamcrest.Matchers.lessThan;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class DeletarSimulacoesTest extends BaseTest {

    SimulacaoRequest simulacao = new SimulacaoRequest();

    @Test
    @Tag("todos")
    @Order(5)
    public void deveDeletarSimulacaoComSucesso() {
        simulacao.deletar(11)
                 .then()
                    .statusCode(HttpStatus.SC_OK)
                    .time(lessThan(2L), TimeUnit.SECONDS)
                    .log().all();
    }
}
