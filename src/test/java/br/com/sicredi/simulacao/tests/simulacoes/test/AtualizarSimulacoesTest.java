package br.com.sicredi.simulacao.tests.simulacoes.test;

import br.com.sicredi.simulacao.tests.base.BaseTest;
import br.com.sicredi.simulacao.tests.simulacoes.request.SimulacaoRequest;
import br.com.sicredi.simulacao.utils.Utils;
import org.apache.http.HttpStatus;
import org.junit.jupiter.api.*;

import java.text.MessageFormat;
import java.util.concurrent.TimeUnit;

import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.Matchers.lessThan;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class AtualizarSimulacoesTest extends BaseTest {

    SimulacaoRequest simulacao = new SimulacaoRequest();

    @Test
    @Tag("todos")
    @Order(4)
    public void deveAtualizarSimulacaoComSucesso() {
        String cpf = Utils.cpfValido();

        simulacao.atualizar(cpf, false)
                 .then()
                    .statusCode(HttpStatus.SC_OK)
                    .time(lessThan(2L), TimeUnit.SECONDS)
                    .log().all();
    }

    @Test
    @Tag("todos")
    @Order(18)
    public void deveValidarAtualizacaoSimulacaoCpfInexistente() {
        String cpfInexistente = Utils.cpfAleatorio(false);

        simulacao.atualizar(cpfInexistente, false)
                .then()
                .statusCode(HttpStatus.SC_NOT_FOUND)
                .body("mensagem", is(MessageFormat.format("CPF {0} não encontrado", cpfInexistente)))
                .time(lessThan(2L), TimeUnit.SECONDS)
                .log().all();
    }

    @Test
    @Tag("todos")
    @Order(19)
    public void deveValidarAtualizacaoSimulacaoComEmailInvalido() {
        String cpf = Utils.cpfValido();

        simulacao.atualizar(cpf, "email-invalido.com", true)
                .then()
                .statusCode(HttpStatus.SC_BAD_REQUEST)
                .body("erros.email", anyOf(equalTo("E-mail deve ser um e-mail válido"), equalTo("não é um endereço de e-mail")))
                .time(lessThan(2L), TimeUnit.SECONDS)
                .log().all();
    }

    @Test
    @Tag("todos")
    @Order(20)
    public void deveValidarAtualizacaoSimulacaoComValorAcima() {
        String cpf = Utils.cpfValido();

        simulacao.atualizar(cpf, 50000.00, true)
                .then()
                .statusCode(HttpStatus.SC_OK)
                .time(lessThan(2L), TimeUnit.SECONDS)
                .log().all();
    }
}
