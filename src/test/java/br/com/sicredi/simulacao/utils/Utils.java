package br.com.sicredi.simulacao.utils;

import com.github.javafaker.Faker;

import java.util.ArrayList;
import java.util.Locale;
import java.util.Random;

public class Utils {

    public static Faker faker = new Faker(new Locale("pt-BR"));

    public static final int VALOR_MAX = 40000;

    public static final int VALOR_MIN = 1000;

    public static final int PARCELA_MAX = 48;

    public static final int PARCELA_MIN = 2;

    private static int randomiza(int n) {
        int ranNum = (int) (Math.random() * n);
        return ranNum;
    }

    private static int mod(int dividendo, int divisor) {
        return (int) Math.round(dividendo - (Math.floor(dividendo / divisor) * divisor));
    }

    public static String cpfAleatorio(boolean comPontos) {
        int n = 9;
        int n1 = randomiza(n);
        int n2 = randomiza(n);
        int n3 = randomiza(n);
        int n4 = randomiza(n);
        int n5 = randomiza(n);
        int n6 = randomiza(n);
        int n7 = randomiza(n);
        int n8 = randomiza(n);
        int n9 = randomiza(n);
        int d1 = n9 * 2 + n8 * 3 + n7 * 4 + n6 * 5 + n5 * 6 + n4 * 7 + n3 * 8 + n2 * 9 + n1 * 10;

        d1 = 11 - (mod(d1, 11));

        if (d1 >= 10)
            d1 = 0;

        int d2 = d1 * 2 + n9 * 3 + n8 * 4 + n7 * 5 + n6 * 6 + n5 * 7 + n4 * 8 + n3 * 9 + n2 * 10 + n1 * 11;

        d2 = 11 - (mod(d2, 11));

        String retorno = null;

        if (d2 >= 10)
            d2 = 0;
        retorno = "";

        if (comPontos)
            retorno = "" + n1 + n2 + n3 + '.' + n4 + n5 + n6 + '.' + n7 + n8 + n9 + '-' + d1 + d2;
        else
            retorno = "" + n1 + n2 + n3 + n4 + n5 + n6 + n7 + n8 + n9 + d1 + d2;

        return retorno;
    }

    public static String cpfValido() {
        ArrayList<String> cpfList = new ArrayList<>();

        //cpfList.add("66414919004");
        cpfList.add("17822386034");

        Random random = new Random();

        int index = 0;
        for (int i = 0; i < cpfList.size(); i++) {
            index = random.nextInt(cpfList.size());
        }

        return cpfList.get(index);
    }

    public static String cpfRestricao() {
        ArrayList<String> cpfList = new ArrayList<>();

        cpfList.add("97093236014");
        cpfList.add("60094146012");
        cpfList.add("84809766080");
        cpfList.add("62648716050");
        cpfList.add("26276298085");
        cpfList.add("01317496094");
        cpfList.add("55856777050");
        cpfList.add("19626829001");
        cpfList.add("24094592008");
        cpfList.add("58063164083");

        Random random = new Random();

        int index = 0;
        for (int i = 0; i < cpfList.size(); i++) {
            index = random.nextInt(cpfList.size());
        }

        return cpfList.get(index);
    }

    public static double geraNumeroDecimalAleatorio(int min, int max) {
        if (min >= max) {
            throw new IllegalArgumentException("Faixa inválida [" + min + ", " + max + "]");
        }
        return min + Math.random() * (max - min);
    }

    public static int geraNumeroInteiroAleatorio(int min, int max) {
        Random random = new Random();
        return random.nextInt(max - min) + min;
    }
}
