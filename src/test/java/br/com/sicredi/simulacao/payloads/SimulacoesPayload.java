package br.com.sicredi.simulacao.payloads;

import br.com.sicredi.simulacao.payloads.model.SimulacoesModel;
import br.com.sicredi.simulacao.utils.Utils;
import com.github.javafaker.Faker;
import com.google.gson.Gson;
import lombok.Data;

@Data
public class SimulacoesPayload {

    public static Faker faker = Utils.faker;

    public static double valor = Utils.geraNumeroDecimalAleatorio(Utils.VALOR_MIN, Utils.VALOR_MAX);

    public static int parcelas = Utils.geraNumeroInteiroAleatorio(Utils.PARCELA_MIN, Utils.PARCELA_MAX);

    public static String toJson(String cpf, boolean seguro) {
        SimulacoesModel simulacoesModel = new SimulacoesModel(cpf, faker.name().fullName(), faker.internet().emailAddress(), valor, parcelas, seguro);
        return new Gson().toJson(simulacoesModel);
    }

    public static String toJson(String cpf, String email, boolean seguro) {
        SimulacoesModel simulacoesModel = new SimulacoesModel(cpf, faker.name().fullName(), email, valor, parcelas, seguro);
        return new Gson().toJson(simulacoesModel);
    }

    public static String toJson(String cpf, double valor, boolean seguro) {
        SimulacoesModel simulacoesModel = new SimulacoesModel(cpf, faker.name().fullName(), faker.internet().emailAddress(), valor, parcelas, seguro);
        return new Gson().toJson(simulacoesModel);
    }

    public static String toJson(boolean cpfPontos, boolean seguro) {
        SimulacoesModel simulacoesModel = new SimulacoesModel(Utils.cpfAleatorio(cpfPontos), faker.name().fullName(), faker.internet().emailAddress(), valor, parcelas, seguro);
        return new Gson().toJson(simulacoesModel);
    }

    public static String toJson(boolean cpfPontos, String email, boolean seguro) {
        SimulacoesModel simulacoesModel = new SimulacoesModel(Utils.cpfAleatorio(cpfPontos), faker.name().fullName(), email, valor, parcelas, seguro);
        return new Gson().toJson(simulacoesModel);
    }

    public static String toJson(boolean cpfPontos, double valor, boolean seguro) {
        SimulacoesModel simulacoesModel = new SimulacoesModel(Utils.cpfAleatorio(cpfPontos), faker.name().fullName(), faker.internet().emailAddress(), valor, parcelas, seguro);
        return new Gson().toJson(simulacoesModel);
    }

    public static String toJson(boolean cpfPontos, int parcelas, boolean seguro) {
        SimulacoesModel simulacoesModel = new SimulacoesModel(Utils.cpfAleatorio(cpfPontos), faker.name().fullName(), faker.internet().emailAddress(), valor, parcelas, seguro);
        return new Gson().toJson(simulacoesModel);
    }
}
