package br.com.sicredi.simulacao.payloads.model;

import lombok.Data;

@Data
public class SimulacoesModel {

    private String cpf;
    private String nome;
    private String email;
    private double valor;
    private int parcelas;
    private String seguro;

    public SimulacoesModel(String cpf, String nome, String email, double valor, int parcelas, boolean seguro) {
        this.cpf = cpf;
        this.nome = nome;
        this.email = email;
        this.valor = valor;
        this.parcelas = parcelas;
        this.seguro = Boolean.toString(seguro);
    }
}
